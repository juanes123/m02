# E1.2 - Conversions de base

## Entrega

Realitzeu (sense ús de cap ajuda electrònica ;-) ) la conversió entre els següents codis. Feu les operacions que necessiteu en el mateix full d'entrega:

| DECIMAL | BINARI          | HEXADECIMAL | OCTAL |
| ------- | --------------- | ----------- | ----- |
| 4       |                 |             |       |
|         | 1 0 1 0 1 0 1 0 |             |       |
|         |                 | ABCD        |       |
|         |                 |             | 754   |
| 127     |                 |             |       |
|         | 1 0 0 0 0 0 0 0 |             |       |
|         |                 | 1           |       |
|         |                 |             | 6     |
| 64      |                 |             |       |
|         | 1 0 0 1 1 1 1 1 |             |       |
|         |                 | c074        |       |
|         |                 |             | 606   |

**CÀLCULS**: 